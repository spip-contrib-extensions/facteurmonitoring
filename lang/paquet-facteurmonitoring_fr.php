<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'facteurmonitoring_description' => 'Vérifie si le facteur envoie bien ses emails',
	'facteurmonitoring_nom' => 'Monitoring du Facteur',
	'facteurmonitoring_slogan' => 'Vérifie que le facteur envoie bien ses courriers',
);

